import json
import socketserver
import threading
import signal
import queue
import logging

import requests
from requests_toolbelt.adapters.source import SourceAddressAdapter

logger = logging.getLogger('qa_proxy')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

logger.info("Read config")
with open('config.json') as config_file:
    config = json.load(config_file)

class Proxy():
    def __init__(self, q, url, apikey, external_facing_ip):
        self.q = q
        self.url = url
        self.apikey = apikey
        self.external_facing_ip = external_facing_ip
        self.logger = logging.getLogger('qa_proxy.proxy')

    # This method is synchronous, if you would like to speed up througput this method should be made asynchronous.
    def run(self):
        s = requests.Session()
        s.mount('https://', SourceAddressAdapter(self.external_facing_ip))
        while True:
            data = q.get()
            self.logger.info("Send message to qa-server. " + str(data))
            headers = {
                "apikey": self.apikey,
                "Content-Type": "application/xml",
            }
            r = s.post(self.url, data = data, headers=headers)
            self.logger.info("Sending message to qa-server resulted in [" + str(r.status_code) + "]")
            q.task_done()


class ThreadedTCPRequestHandler(socketserver.StreamRequestHandler):
    def handle(self):
        # self.request is the TCP socket connected to the client
        self.logger = logging.getLogger('qa_proxy.tcpserver')
        while True:
            self.data = self.rfile.readline()
            if not self.data:
                break
            self.data = self.data.strip()
            self.server.q.put(self.data)
            self.logger.info("Received message from: " + str(self.client_address[0]))
            self.logger.info(str(self.data))

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

logger.info("Init proxy.")
q = queue.Queue()
proxy = Proxy(q, config["qa-server"], config["qa-server-apikey"], config["external-facing-ip"])

t = threading.Thread(target=proxy.run)
t.start()

if __name__ == "__main__":
    # Create the server, binding to addresses specified in config file.
    logger.info("Start internal TCP server.")
    server = ThreadedTCPServer((config['internal-facing-ip'], config['internal-facing-port']), ThreadedTCPRequestHandler)
    server.q = q

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()

    signal.pause()
    server_thread.stop()
    server_thread.join()