# qa-proxy

De qa-proxy is ontwikkeld om data vanuit de wisselautomaten van Bombardier vanuit het interne netwerk op servers op een publiek netwerk te krijgen.

Korte gebruikshandleiding.
1. Zorg er voor dat python geïnstalleerd, deze versie is ontwikkeld voor python3.7
2. Installeer benodigde libraries met pip install -r requirements.txt
3. Kopieer sample_config.json naar config.json en pas paramaters aan op basis QA Solution Design / Plan of Action
4. Draai het programma met behulp van python main.py
